

// prototype inherit
console.log('Prototype Inherit');

function Vehicle(type, color){
	this.type = type;
	this.color = color;
	// console.log('My',this.color,this.type,'is started...');
}
Vehicle.prototype.goLeft = function(){
	console.log('My',this.color,this.type,'is going Left');
}
Vehicle.prototype.goRight = function(){
	console.log('My',this.color,this.type,'is going Right');
}
Vehicle.prototype.goForward = function(){
	console.log('My',this.color,this.type,'is going Forward');
}
Vehicle.prototype.goBackward = function(){
	console.log('My',this.color,this.type,'is going Back');
}



// function MyRide(...args){
	// Vehicle.apply(this, args);
// }
function MyRide(type,color){
	Vehicle.call(this, type,color);
}
MyRide.prototype = Object.create(Vehicle.prototype);
MyRide.prototype.stop = function(){
	console.log('My',this.color,this.type,'reached at the destination...Please Stop!');
}
MyRide.prototype.goLeft = function(){
	console.log('My Ride',this.color,this.type,'is going Left');
}
MyRide.prototype.constructor=MyRide;


var bike = new MyRide("Bike", "Red");
var car = new MyRide("Car", "Red");
console.log(bike);
// console.log(car);

/*
bike.goLeft();
bike.goForward();
bike.goRight();
bike.goBackward();
bike.stop();
*/

/*
function Vehicle(type,color){
	this.type = type;
	this.color = color;
	this.print = function(){ 
		return 'My '+this.type+' color is '+this.color;
	}
}
function MyRide(type,color){
	Vehicle.call(this, type, color);
}
var bike = new MyRide('Bike', 'red');
var car = new MyRide('car', 'blue');
console.log(bike.print());
console.log(car.print());
*/



// Factory Function
console.log('Factory Function');
var move = {
	goLeft(){
		console.log('My',this.color,this.type,'is going Left');
	},
	goRight(){
		console.log('My',this.color,this.type,'is going Right');
	},
	goForward(){
		console.log('My',this.color,this.type,'is going Forward');
	},
	goBackward(){
		console.log('My',this.color,this.type,'is going Back');
	}
};


var myRideByFactory = (type, color) => Object.assign(Object.create(move), {
	type,
	color,
	stop(){
		console.log('My',this.color,this.type,'reached at the destination...Please Stop!');
	},
	// goLeft(){
		// console.log('My FF',this.color,this.type,'is going Left');
	// }
});

var bikeByFactory = myRideByFactory('Bike', 'red');
console.log(bikeByFactory);

/*
bikeByFactory.goLeft();
bikeByFactory.goForward();
bikeByFactory.goRight();
bikeByFactory.goBackward();
bikeByFactory.stop();
*/


// const proto = {
  // hello: function hello() {
    // return `Hello, my name is ${ this.name }`;
  // }
// };

// const geoge = Object.assign({}, proto, {name: 'George'});

// const msg = geoge.hello();

// console.log(geoge); // Hello, my name is George




