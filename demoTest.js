/*

new javascript problems:
------------------------

1.
var x = function(p) {
 return p+p;
};
console.log(x);
now rewrite the function so that you can call the function inside x.


2.
create a javascript object
- populate it with 5 key values.
- print in console the key value pairs.
- remove third key.
- change fourth key value.
- change fifth key and value.
- print in console the object.


3.
Get area of a circle.
Get area of a rectangle. The width of rectangle is same as the radius of the circle. The height of rectangle is twice as of the width;
Now compare which area is greater?
Note : must use object class.

4.
Create a circle object with radius 10 and keep a option to calculate the area of the circle. Now create a sphere object from the circle object.Now set a method in the sphere object in such a way, when you call below:
sphere.method(arg) // here arg must be a function.
it will return the difference between the area of circle and the sphere.
Note: you can’t use prototype or class.


*/


/*
===================================================
	Ans 1
===================================================
*/


var x = (function(p) {
 return p+p;
}(20));
// console.log(x);

/*
===================================================
	Ans 2
===================================================
*/

var obj = {};
for(let i=1;i<=5;i++){
	obj['objElement'+i] = 'Data'+i;
}
// console.log('\nKey Value pair: \n',obj);



/*
for(let i=1;i<=5;i++){
	if(i == 3){
		delete obj['objElement'+i];
	}
}
console.log('\nThird key removed: \n',obj);
*/



for(let i=1;i<=5;i++){
	if(i == 4){
		obj['objElement'+i] = 'DataChanged'+i;
	}
}
// console.log('\nKey value changed: \n',obj);


for(let i=1;i<=5;i++){
	if(i == 5){
		delete obj['objElement'+i];
		obj['objElementChanged'+i] = 'DataChanged'+i;
	}
}
// console.log('\nKey & value changed: \n',obj);


/*
===================================================
	Ans 3
===================================================
*/


// Using Object Prototype
function Geometry(radius){
	this.width = radius;
	this.height = radius*2;
}
Geometry.prototype.circleArea = function(){
	return 3.1416 * this.width * this.width;
}
Geometry.prototype.rectangleArea = function(){
	return this.width * this.height;
}
Geometry.prototype.compare = function(){
	var msg;
	var circle = 3.1416 * this.width * this.width;
	var rectangle = this.width * this.height;
	if(circle > rectangle){
		msg = "Area of the circle is bigger than area of rectangle";
	}else if(circle < rectangle){
		msg = "Area of the circle is smaller than area of rectangle";
	}else {
		msg = "Area of the circle is equal to the area of rectangle";
	}
	return msg;
}
// Using Object Prototype Ends

var geo = new Geometry(10);
// console.log('\nArea of Circle: ',geo.circleArea());
// console.log('\nArea of Rectangle: ',geo.rectangleArea());
// console.log('\nComparison: ',geo.compare());



/*
===================================================
	Ans 4
===================================================
*/
/*
function difference(func1,func2){
	let differ = func1 - func2;
	return differ;
}

function copyObj(srcObj){
	return Object.assign({},srcObj);
}

var circle = {
	radius :  10,
	calArea: function (){
		return Math.PI * this.radius * this.radius;
	}
}

var sphere = copyObj(circle);

sphere.calArea = function(){
	return 4 * Math.PI * this.radius * this.radius;
};

sphere.compare=function(func){
	return func(sphere.calArea(),circle.calArea());
}

console.log('The Difference ',sphere.compare(difference))
*/	
	
/*
===================================================
	Ans 4 using Prototype
===================================================
*/

/*
function Circle(){
	this.radius = 10;
}
Circle.prototype.calArea = function(){
	return Math.PI * this.radius * this.radius;
}

function Sphere(){
	Circle.call(this);
}


Sphere.prototype = Object.create(Circle.prototype);

Sphere.prototype.calArea = function(){
	return 4 * Math.PI * this.radius * this.radius;
}

var sphere = new Sphere();
var circle = new Circle();
Sphere.prototype.compare = function(func){
	return func(sphere.calArea(),circle.calArea())
	// return areaOfCircle;
	// return areaOfSphere;
}

function difference(func1,func2){
	let differ = func1 - func2;
	return differ;
}

var s = new Sphere();

console.log('Using Prototype...The Difference ',s.compare(difference))
*/

/*
===================================================
	Ans 4 using Class
===================================================
*/

class Circle{
	constructor(radius){
		this.radius = 10;
	}
	calArea (){
		return Math.PI * this.radius * this.radius;
	}
}

class Sphere extends Circle{
	constructor(radius){
		super(radius)
	}
	calArea (){
		return 4 * super.calArea();
	}
	compare(func){
		return func(s.calArea(),c.calArea())
	}
}

function difference(func1,func2){
	let differ = func1 - func2;
	return differ;
}

var s = new Sphere();
var c = new Circle();


console.log('Using Class...The Difference ',s.compare(difference))








