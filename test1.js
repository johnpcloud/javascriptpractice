/*
function C () {
	
}
C.prototype.calc = function (a, b) {
    return a + b;
}

// Create 2 instances:
var ins1 = new C(),
    ins2 = new C();

// Test the calc method:
console.log(ins1.calc(1,1), ins2.calc(1,1));


// Change the prototype method
C.prototype.calc = function () {
    var args = Array.prototype.slice.apply(arguments),
        res = 0, c;

    while (c = args.shift())
        res += c;

    return res; 
}

// Test the calc method:
console.log(ins1.calc(1,1,1), ins2.calc(1,1,1));
*/

function C () {
	
this.calc = function (a, b) {
    return a + b;
};}

var ins1 = new C(),
    ins2 = new C();
console.log('Constructor :', ins1.calc(1,1), ins2.calc(1,1));

// Change the prototype method
C.prototype.calc = function () {
    var args = Array.prototype.slice.apply(arguments),
        res = 0, c;

    while (c = args.shift())
        res += c;

    return res; 
}

// Test the calc method:
console.log(ins1.calc(1,1,1), ins2.calc(1,1,1));