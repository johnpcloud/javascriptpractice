
/*
============================================
	Date in JavaScript
============================================
*/
 
//var then = new Date(2010, 0, 1); // The 1st day of the 1st month of 2010
var later = new Date(2010, 0, 1, // Same day, at 5:10:30pm, local time
17, 10, 30);
//var now = new Date(); // The current date and time
//var elapsed = now - then; // Date subtraction: interval in milliseconds
var r,re;
later.getFullYear() // => 2010
later.getMonth() // => 0: zero-based months
later.getDate() // => 1: one-based days
later.getDay() // => 5: day of week. 0 is Sunday 5 is Friday.
later.getHours() // => 17: 5pm, local time
later.getUTCHours() // hours in UTC time; depends on timezone
later.toString() // => "Fri Jan 01 2010 17:10:30 GMT-0800 (PST)"
later.toUTCString() // => "Sat, 02 Jan 2010 01:10:30 GMT"
later.toLocaleDateString() // => "01/01/2010"
r=later.toLocaleTimeString() // => "05:10:30 PM"
later.toISOString() // => "2010-01-02T01:10:30.000Z"; ES5 only

//console.log(r);


/*
===============================================
	String in JavaScript
===============================================
*/

var s = "hello, world" // Start with some text.
s.charAt(0) // => "h": the first character.
s.charAt(s.length-1) // => "d": the last character.
s.substring(1,4) // => "ell": the 2nd, 3rd and 4th characters.
s.slice(1,4) // => "ell": same thing
s.slice(-3) // => "rld": last 3 characters
s.indexOf("l") // => 2: position of first letter l.
s.lastIndexOf("l") // => 10: position of last letter l.
s.indexOf("l", 3) // => 3: position of first "l" at or after 3
s.split(", ") // => ["hello", "world"] split into substrings
s.replace("h", "H") // => "Hello, world": replaces all instances
s.toUpperCase() // => "HELLO, WORLD"

//console.log(r);

var jsObjects = [
   {a: 1, b: 2}, 
   {a: 3, b: "name"}, 
   {a: 5, b: "name"}, 
   {a: 7, b: 8}
];
var result = jsObjects.filter(obj => {
  return obj.b === "name"
})
//console.log(result);

var myObject = [
	{
		label: 'foo',
		name: 'bar',
		id: 12
	},
	{
		label: 'name',
		name: 'john',
		id: 1254
	}
];

//Object.assign(myObject[0], {label: 'Test', name: 'Barbar'})
//console.log(myObject)

/*
// new object push to array of object
var newObj = {
	label: 'new name',
	name: 'My new Name'
}
myObject.push(newObj);
//console.log(myObject);

// Slicing a object from array of objects
myObject = myObject.slice(1);
// console.log(myObject);
*/

/*
====================================================================================
	Javascript get set
====================================================================================
*/

/*
var p = {
	// x and y are regular read-write data properties.
	x: 3.0,
	y: 3.0,
	// r is a read-write accessor property with getter and setter.
	// Don't forget to put a comma after accessor methods.
	get r() { 
		return Math.sqrt(this.x*this.x + this.y*this.y); 
	},
	get aaa() { 
		return this.x + this.y; 
	},
	set r(newvalue) {
		var oldvalue = this.x + this.y;
		var ratio = newvalue/oldvalue;
		this.x *= ratio;
		this.y *= ratio;
	},
	// theta is a read-only accessor property with getter only.
	get theta() { 
		return Math.atan2(this.y, this.x); 
	}
};
*/

// var q = Object.create(p); // Create a new object that inherits getters and setters
// q.x = 6, q.y = 6; // Create q's own data properties
// console.log(q.r); // And use the inherited accessor properties
// console.log(q.theta);
// console.log(q.aaa);

/*
============================================
2.
create a javascript object
- populate it with 5 key values.
- print in console the key value pairs.
- remove third key.
- change fourth key value.
- change fifth key and value.
- print in console the object.
============================================
*/

/*
var obj = {};
for(let i=1;i<=5;i++){
	obj['objElement'+i] = 'Data'+i;
}
// console.log('\nKey Value pair: \n',obj);
*/
/* 
delete obj.objElement3;
console.log('\nThird key removed: \n',obj);
*/
/*
function renameObjectKey(obj, old_key, new_key) {    

	if (old_key !== new_key) {                  
		Object.defineProperty(obj, new_key,
			Object.getOwnPropertyDescriptor(obj, old_key)
		); 
		delete obj[old_key];
	} 
}

obj.forEach(renameObjectKey(obj,'objElement4','elementChanged4'));
// console.log('\nKey changed: \n',obj);
*/

/*
===================================================
3.
Get area of a circle.
Get area of a rectangle. The width of rectangle is same as the radius of the circle. The height of rectangle is twice as of the width;
Now compare which area is greater?
Note : must use object class / Object Prototype
===================================================
*/




/*
// Using Class

class Geometry{
	constructor (radius){
		this.width = radius;
		this.height = radius*2;
	}
	circleArea(){
		return 3.1416 * this.width * this.width;
	}
	rectangleArea(){
		return this.width * this.height;
	}
	compare(){
		var msg;
		var circle = 3.1416 * this.width * this.width;
		var rectangle = this.width * this.height;
		if(circle > rectangle){
			msg = "Area of the circle is bigger than area of rectangle";
		}else if(circle < rectangle){
			msg = "Area of the circle is smaller than area of rectangle";
		}else {
			msg = "Area of the circle is equal to the area of rectangle";
		}
		
		return msg;
	}
}

//Using class ends
*/

/*
// Using Object Prototype
function Geometry(radius){
	this.width = radius;
	this.height = radius*2;
}
Geometry.prototype.circleArea = function(){
	return 3.1416 * this.width * this.width;
}
Geometry.prototype.rectangleArea = function(){
	return this.width * this.height;
}
Geometry.prototype.compare = function(){
	var msg;
	var circle = 3.1416 * this.width * this.width;
	var rectangle = this.width * this.height;
	if(circle > rectangle){
		msg = "Area of the circle is bigger than area of rectangle";
	}else if(circle < rectangle){
		msg = "Area of the circle is smaller than area of rectangle";
	}else {
		msg = "Area of the circle is equal to the area of rectangle";
	}
	return msg;
}
// Using Object Prototype Ends



var geo = new Geometry(10);
// console.log('\nArea of Circle: ',geo.circleArea());
// console.log('\nArea of Rectangle: ',geo.rectangleArea());
// console.log('\nComparison: ',geo.compare());

*/




/*
function loadStuff(callback) {
    // Go off and make an XHR or a web worker or somehow generate some data
    var data = 'My Name'
    callback(data);
}

loadStuff(function(data){
    console.log(data);
});
*/


/*
function Range(from, to) {
this.from = from;
this.to = to;
}

Range.prototype = {
	
	includes: function(x) { return this.from <= x && x <= this.to; },

	foreach: function(f) {
		for(var x = Math.ceil(this.from); x <= this.to; x++) f(x);
	},

	toString: function() { return "(" + this.from + "..." + this.to + ")"; }
};

var r = new Range(1,3); // Create a range object
r.includes(2); // => true: 2 is in the range
r.foreach(console.log); // Prints 1 2 3
console.log(r); // Prints (1...3)
*/





// We have some behaviors
const canSayHi = self => ({
  sayHi: () => console.log(`Hi! I'm ${self.name}`)
});
const canEat = () => ({
  eat: food => console.log(`Eating ${food}...`)
});
const canPoop = () => ({
  poop: () => console.log('Going to 💩...')
});

// Combined previous behaviours
const socialBehaviors = self => Object.assign({}, canSayHi(self), canEat(), canPoop());

const alligator = name => {
  const self = {
    name
  };

  const alligatorBehaviors = self => ({
    bite: () => console.log("Yum yum!")
  });

  return Object.assign(self, socialBehaviors(self), alligatorBehaviors(self));
};


const jack = alligator("jack");
jack.sayHi(); // Hi! I'm jack
jack.eat("Banana"); // Eating Banana...
jack.bite(); // Yum yum!















