

let subscribers = {};

module.exports = {
	publish(event, student) {
		if (!subscribers[event]) 
			return;

		subscribers[event].forEach(subscriberCallback => subscriberCallback(student));
	},
  
	subscribe(event, callback) {
		let index;

		if (!subscribers[event]) {
		  subscribers[event] = [];
		}

		index = subscribers[event].push(callback) - 1;

		return {
			unsubscribe() {
				subscribers[event].splice(index, 1);
			}
		};
	}
  
};

/*
//====================================================================================================


// var events = (function(){
	var topics = {};
	var hOP = topics.hasOwnProperty;

	// return {
		 function subscribe(topic, listener) {
			// Create the topic's object if not yet created
			if(!hOP.call(topics, topic)) topics[topic] = [];

			// Add the listener to queue
			var index = topics[topic].push(listener) -1;

			// Provide handle back for removal of topic
			return {
				remove: function() {
					delete topics[topic][index];
				}
			};
		}
		function publish(topic, info) {
			// If the topic doesn't exist, or there's no listeners in queue, just leave
			if(!hOP.call(topics, topic)) return;

			// Cycle through topics queue, fire!
			topics[topic].forEach(function(item) {
				item(info != undefined ? info : {});
			});
		}
	// };
// })();


//========================================================================================================
*/

// var pubsub = {
	// var subscribers = {};

	// publish:function (event, data){
		// if(!subscribers[event]) return;
		
		// subscribers[event].forEach(function(item){
			// item(info != undefined ? info : {});
		// });
		
		// console.log(subscribers);
	// },
	
	// subscribe:function (event, callback){
		
		// var index;
		
		// if(!subscribers[event]) 
			// subscribers[event]=[];
		
		// index = subscribers[event].push(callback) - 1;;
		
		// console.log(subscribers);
	// }
// }



// var events = {
                // events: {},
		// subscribe: function (eventName, object, callback) {
			// this.events[eventName] = this.events[eventName] || [];
			// this.events[eventName].push({object: object, callback: callback});
		// },
		// unsubscribe: function(eventName, object, callback) {
			// if (this.events[eventName]) {
				// for (var i = 0; i < this.events[eventName].length; i++) {
					// if (this.events[eventName][i].object === object) {
					  // this.events[eventName].splice(i, 1);
					  // break;
					// }
				// };
			// }
		// },
		// publish: function (eventName, data) {
			// if (this.events[eventName]) {
				// this.events[eventName].forEach(function(instance) {
					// instance.callback(data);
				// });
			// }
		// }
// };