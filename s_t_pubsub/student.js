const pubSub = require("./pubsub");


var student = [
	{
		name: "john",
		roll: 2016001001
	},
	{
		name: "robin",
		roll: 2016002002
	},
	{
		name: "rony",
		roll: 2016003003
	},
];

student[1].name = "Jemmy";

module.exports = {
	publishEvent() {
		const data = student;

		pubSub.publish("studentChange", data);
	}
};

