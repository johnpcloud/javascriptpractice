var people = [
    {
        "name": "Jack",
        "age": 30
    },
    {
        "name": "Trump",
        "age": 35
    },
    {
        "name": "Smith",
        "age": 25
    },
    {
        "name": "Trump",
        "age": 35
    },
    {
        "name": "Bill",
        "age": 45
    },
    {
        "name": "Hilary",
        "age": 27
    },
    {
        "name": "Smith",
        "age": 25
    }
]

function getUnique(arr, comp) {
	
  const unique = arr
       .map(e => e[comp])

     // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e]).map(e => arr[e]);

   return unique;
}

var uniquePeople = getUnique(people,"name");

console.log('List of Unique named people:\n',uniquePeople);



function compare( a, b ) {
  if ( a.age > b.age){
    return -1;
  }
  if ( a.age < b.age ){
    return 1;
  }
  return 0;
}
console.log('\nList of Age decending named people:\n',uniquePeople.sort( compare ));

