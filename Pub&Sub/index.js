const publisher = require("./publisher");
const subscriber = require("./subscriber");

var student = [
	{
		name: "john",
		roll: 2016001001
	},
	{
		name: "robin",
		roll: 2016002002
	},
	{
		name: "rony",
		roll: 2016003003
	},
];

student[1].name = "Jemmy";


// We use student's publishEvent() method
publisher.publishEvent("StudentChange",student);

