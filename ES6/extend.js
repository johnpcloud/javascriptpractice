/*
class Person{
	constructor(name ,age,weight){
		this.name = name;
		this.age = age;
		this.weight = weight;
	}
	displayName(){
		console.log(this.name);
	}
	displayAge(){
		console.log(this.age);
	}
	displayWeight(){
		console.log(this.weight);
	}
}

class Programmer extends Person{
	constructor(name ,age,weight,language){
		super(name,age,weight);
		this.language = language;
	}
	displayLanguage(){
		console.log(this.language);
	}
}

let john = new Programmer('john', 20, 70, 'javascript');

john.displayName();
john.displayAge();
john.displayWeight();
john.displayLanguage();
*/


/*
function Person (name ,age,weight) {
		this.name = name;
		this.age = age;
		this.weight = weight;
	
	this.displayName = function() {
		console.log(this.name);
	},
	this.displayAge = function() {
		console.log(this.age);
	},
	this.displayWeight = function() {
		console.log(this.weight);
	}
}
let john = new Person('john', 20, 70);

john.displayName();
john.displayAge();
john.displayWeight();
// john.displayLanguage();
*/

/*
//	Extending data to child from parent using custom extend function
function extend(child, parent){
    for(var key in parent){
        if(parent.hasOwnProperty(key)){
            child[key] = parent[key];
		}
	}
    return child;
}

// var p = {pName: 'Parent Name'};
var c = {dob: 'Child Birthdate', age: 10, gender: 'Male'};

// var s = extend(c, p);
var s = extend(c, {name: 'Parent Name'});
// console.log(s);
*/


class Parent {
	constructor(emo1,emo2,emo3){
		this.emo1 = emo1;
		this.emo2 = emo2;
		this.emo3 = emo3;
	}
	extend(data){
		for(let key in data){
			if(data.hasOwnProperty(key)){
				this[key] = data[key];
			}
		}
		return this;
	}
	
	// extend(data){
		// let extendedKeyValue = {};
		
		// for(let key in this){
			// if(this.hasOwnProperty(key)){
				// extendedKeyValue[key] = this[key];
			// }
			
		// }
		// for(let key in this){
			// if(this.isPrototypeOf(key)){
				// extendedKeyValue[key] = this[key];
			// }
		// }
		
		// for(let key in data){
			// if(data.hasOwnProperty(key)){
				// extendedKeyValue[key] = data[key];
			// }
		// }
		// for(let key in data){
			// if(this.isPrototypeOf(key)){
				// extendedKeyValue[key] = this[key];
			// }
		// }
		// return extendedKeyValue;
	// }
	
	// displayemo1(){
		// console.log(this.emo1);
	// }
	// displayemo2(){
		// console.log(this.emo2);
	// }
	// displayemo3(){
		// console.log(this.emo3);
	// }
}

var p = new Parent('protective', 'caring', 'strict');

console.log('\n=================\n');	
console.log('Type of Parent: ',typeof(p));
console.log('Before using extend\n',p);

// var child = p.extend({ emotion1 : 'naughty'});
var child = p.extend({ emotion1 : 'naughty'});


console.log('\n=================\n');
console.log('After using extend\n',p);
console.log('\n=================\n');
console.log('Type of child: ',typeof(child));
// console.log(child.emo2);
// console.log(child.emotion1);
// child.displayemo3();

