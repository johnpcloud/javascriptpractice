/*
var person = {
	name : 'My name',
	age : 20,
	get getName(){
		return this.name;
	},
	set getName(name){
		return this.name;
	}
}
var p = Object.create(person);
// p.name = 'My New Name';
// console.log(p.getName);
*/
//========================================================
/*
class Person {
	constructor(name){
		this.name = name;
	}
	// getName(){
		// var param = this.name;
		// var x = (function (n) { 
            // return n; 
        // })(param);
		// console.log(x);
	// }
}
var p = new Person('Class Name');
// p.name = 'My New Name';
// console.log(p.name);
p.getName();
*/
//=========================================================

var f = {
	a : 'apple',
	b : 'banana',
	c : 'Cherries',
	d : 'Dragon Fruit'
};



(function( o ){
	var temp = {};
    for(let key in o){
		temp['get'+key.toUpperCase()] = function(){return o[key];}	
	}
	Object.assign(o, temp);
})( f )


console.log( f.getA() );
console.log( f.getB() );
console.log( f.getC() );
console.log( f.getD() );

//==============================================================
// here a namespace object is passed as a function 
// parameter, where we assign public methods and
// properties to it
/*
var namespace = namespace || {};
(function( o ){    
    o.foo = "foo";
    o.bar = function(){
        return "bar";    
    };
})( f )

console.log( f );
*/
//===============================================================

/*
f.superCool();

console.log(f.superCool.name) ;
/*
/*
var w = (function(){
	for(var key in f){
		// console.log(key);
		console.log(f['get' + key]);
		// f["get" + key] = function() { return f.key; };
	}
	// var greeting = 'Welcome to blog';
	// console.log(greeting); //Output: Welcome to blog
})();
*/
/*
(function(){
	// for(var key in f){
		// console.log(key);
		// console.log(f['get' + key]);
		// f["get" + key] = function() { return f.key; };
	// }
	// var greeting = 'Welcome to blog';
	console.log('Welcome to blog'); //Output: Welcome to blog
})();
*/

// console.log(w);

// f.getA();

/*
let obj = {a:4, b:6};
let cloneObj ={ name: 'Krunal', surname: 'Lathiya' }
 Object.assign(obj,cloneObj);
console.log(obj);
*/
