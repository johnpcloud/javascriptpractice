class Parent {
	constructor(emo1,emo2,emo3){
		this.emo1 = emo1;
		this.emo2 = emo2;
		this.emo3 = emo3;
	}
	extend(data){
		for(let key in data){
			if(data.hasOwnProperty(key)){
				this[key] = data[key];
			}
		}
		return this;
	}
}

var p = new Parent('protective', 'caring', 'strict');
console.log('\n=================\n');
console.log('Type of Parent: ',typeof(Parent));
console.log(Parent);	
// console.log("object instance : ", p);
// var c = 
Parent.extend(
	{ 
		emo4 : 'naughty',
		testFunc:function(){
			console.log('test function working');
		}
	}
);

// var c = new Parent();

console.log('\n=================\n');
console.log(Parent);
console.log('\n=================\n');
// console.log(c);
// console.log(p.emo2);
// console.log(Parent.emo2);
// Parent.testFunc();

