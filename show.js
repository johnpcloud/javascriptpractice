var circle = {
	radius :  10,
	calArea: function (){
		return Math.PI * this.radius * this.radius;
	}
}


var sphere = Object.assign({},circle);

sphere.calArea = function(){
	return 4 * Math.PI * this.radius * this.radius;
};

sphere.compare=function(func){
	return func();
}


function difference(){
	let differ = sphere.calArea() - circle.calArea();
	return differ;
}

console.log('The Difference ',sphere.compare(difference))