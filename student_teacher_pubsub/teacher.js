const pubSub = require("./pubsub");
const student = require("./db");

// console.log(student);


student[1].name = "Jemmy";

module.exports = {
	publishEvent() {
		const data = student;

		pubSub.publish("studentChange", data);
	}
};


// let subscription;

// subscription = pubSub.subscribe("studentChange", data => {
  // console.log(data);
  // subscription.unsubscribe();
// });


