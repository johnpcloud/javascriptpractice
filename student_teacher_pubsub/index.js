const student = require("./student");
const teacher = require("./teacher");

// We use teacher's publishEvent() method
teacher.publishEvent();