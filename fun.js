const someFunct = (data) => {console.log('i am a function with data', data)}
const someOtherFunct = (data) => {console.log('i am a function with other data', data)}

const arrayOfFunctions = [someFunct, someOtherFunct];

const data = "some data";

arrayOfFunctions.forEach(fun => fun(data));


// const exe = (somefunc)=>somefunc(data)}
// arrayOfFunctions.forEach(exe);
